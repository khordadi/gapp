GAPP_DIR := $(shell pwd)
CLASS_PATH := $(GAPP_DIR)

GRAAL_DIR := $(HOME)/graal
SVM_DIR := $(GRAAL_DIR)/substratevm

JAVAC := $(JAVA_HOME)/bin/javac
JAVA := $(JAVA_HOME)/bin/java

MX := $(HOME)/opt/mx/mx
SVM := $(MX) native-image

LLVM_BACKEND_FLAGS := -H:CompilerBackend=llvm -H:TempDirectory=$(GAPP_DIR) -H:LLVMMaxFunctionsPerBatch=0
LOG_FLAGS := -H:Log=InvokeCC
SVM_FLAGS := $(LLVM_BACKEND_FLAGS) $(LOG_FLAGS) -cp $(CLASS_PATH)

BINARY_PATH := $(GAPP_DIR)/hello

javac: Hello.java
	$(JAVAC) Hello.java

java: javac
	$(JAVA) Hello

svm: javac
	cd $(SVM_DIR) && $(SVM) $(SVM_FLAGS) Hello $(BINARY_PATH)

exec: svm
	$(BINARY_PATH)

clean:
	rm -rf SVM-* Hello.class hello*